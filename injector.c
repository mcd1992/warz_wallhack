#include <stdio.h>
#include <windows.h>
#include <Tlhelp32.h>

#define INJ_DLL_NAME "tsoverlay.dll"	//Name of dll to inject


void* dllRemoteStrPtr;					//Address (in the remote proc) of fullDllPath by VirtualAllocEx()
char fullDllPath[MAX_PATH];				//Char array that contains our full path to INJ_DLL_NAME
char fullExePath[MAX_PATH];
STARTUPINFO startupInfo;
PROCESS_INFORMATION procInfo;
DWORD injDllBaseAddr;					//Base addr of our injected dll
HANDLE remoteThread, remoteProc;		//Handle to remote thread and process, the one being injected into.
HMODULE kern32dll;						//Contains (pointer not handle) to the kernel32.dll

// Below function takes exe name and returns the pid of the running exe.
DWORD findProcess(char* name){
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	
	if (Process32First(snapshot, &entry) == TRUE){
		while(Process32Next(snapshot, &entry) == TRUE){
			if ( stricmp(entry.szExeFile, name) == 0){
				CloseHandle(snapshot);
				return entry.th32ProcessID;
			}
		}
	}
}

int cleanExit(){
	VirtualFreeEx( remoteProc, dllRemoteStrPtr, sizeof(fullDllPath), MEM_RELEASE ); //Free the memory allocated for dll string in remote proc
	remoteThread = CreateRemoteThread(remoteProc, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(kern32dll,"FreeLibrary"), (void*)injDllBaseAddr, 0, NULL);
	WaitForSingleObject( remoteThread, INFINITE );	//Wait until our injected dll fully unloads
	CloseHandle(remoteProc);						//Close remote process handle
	CloseHandle(remoteThread);						//Close remote thread handle
	printf("Unload Successfull\n");
	return 0;
}

int main(){
	DWORD procId;
	kern32dll = GetModuleHandle("Kernel32");//Base addr of kernel32.dll, needed to call LoadLibraryA and FreeLibrary
	GetFullPathName(INJ_DLL_NAME, MAX_PATH, fullDllPath, NULL); //Get the full path of the dll to inject
	printf("Using %s as the inject dll\n",fullDllPath);
	procId = findProcess("WarZ.exe");	//Get the PID of our process to be injected into
	if (procId==0){ printf("Cannot find process!\n"); exit(0); }
	
	remoteProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, procId); //Create handle to the remote process, being injected into.
	dllRemoteStrPtr = VirtualAllocEx( remoteProc, NULL, sizeof(fullDllPath), MEM_COMMIT, PAGE_READWRITE );	//Allocating memory to inject the dll string into
	WriteProcessMemory( remoteProc, dllRemoteStrPtr, (void*)fullDllPath, sizeof(fullDllPath), NULL );		//Write full dll path into newly allocated memory

	remoteThread = CreateRemoteThread(remoteProc, NULL, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(kern32dll,"LoadLibraryA"), dllRemoteStrPtr, 0, NULL);
	//The above code. Its getting the address of the LoadLib function in kernel32.dll and causing the remote proc to call that, basically.
	WaitForSingleObject( remoteThread, INFINITE ); // Wait untill the remote dll injection thread returns.
	GetExitCodeThread( remoteThread, &injDllBaseAddr ); // This will be used to un-inject (FreeLibrary) our dll later.
	printf("Injected DLL Base Addr: 0x%x\n",(void*)injDllBaseAddr);

	printf("Type 'x' then enter to unload dll\n");
	while(getchar()!='x'){ Sleep(250); }

	return cleanExit();
}