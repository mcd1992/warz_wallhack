#define _WIN32_WINNT 0x500
#include <windows.h>
#include <stdio.h>
#include <Tlhelp32.h>
#include <conio.h>
#include <d3d9.h>
#include <d3dx9.h>

// CE Render Search: A1 ? ? ? ? 8B 40 50 8B 08 8B 91 D8 01 00 00 68 ? ? ? ? 6A 08 50
#define RENDERCLASS_OFFSET 0xD708FC

#define RED D3DCOLOR_ARGB(255,255,40,40)
#define GRN D3DCOLOR_ARGB(255,40,255,40)
#define BLU D3DCOLOR_ARGB(255,40,40,255)

// http://unknowncheats.me/forum/infestation-survivor-stories has some of the source code used in here.

LPDIRECT3DDEVICE9 pDevice = NULL;
D3DVIEWPORT9 Viewport;
LPD3DXFONT pFont = NULL;
DWORD dwProtect = 0;
PDWORD pVTable = NULL;
D3DRECT RectA,RectB;
HANDLE patchThread;
PDWORD dwEBX = 0;
DWORD lastReturnAddr = 0x0;
int COUNTER = 0;
bool somethingIsOn = false;
bool varsTrue = true;

struct VARIABLE_STATUS {
	bool treesVar;
	bool grassVar;
	bool ocullVar;
} varStatus;

class d3renderClass {
	public:
		unsigned char pad_01[0x8];
		HWND hwnd;
		unsigned char pad_02[0xC];
		int windowW;
		int windowH;
		unsigned char pad_03[0x14];
		HWND hwnd2;
		unsigned char pad_04[0x8];
		int refRate;
		unsigned char pad_05[0xC];
		void *pD3dev;
};

class internalGameVarsClass {
	public: 
		template<typename T> void Set(T v) { *(T*)((DWORD)this + 0x184) = v; }
		template<typename T> T Get() { return *(T*)((DWORD)this + 0x184); }
		template<typename T> T GetDefault() { return *(T*)((DWORD)this + 0x188); }
		template<typename T> T Min() { return *(T*)((DWORD)this + 0x18C); }
		template<typename T> T Max() { return *(T*)((DWORD)this + 0x190); }
		char* Name() { return (char*)((DWORD)this); }
		char* Desc() { return (char*)((DWORD)this + 0x84); }
}; 

class gameVarsClass {
	public:
		template<typename T> void Set(T v) { Internal()->Set<T>(v); }
		template<typename T> T Get() { return Internal()->Get<T>(); }
		inline internalGameVarsClass* Internal() { return *(internalGameVarsClass**)((DWORD)this + 0x94); }
		inline gameVarsClass* Next() { return *(gameVarsClass**)((DWORD) this + 0x8C); }
};

class gameVarsList {
	public:
		gameVarsClass* firstVariable() { return (gameVarsClass*) *(DWORD*)((DWORD)this + 0x8008); }
		gameVarsClass* Get(char* name) {
			gameVarsClass* pVar = firstVariable();
			while(pVar && pVar->Internal()){
				if(!strcmp(pVar->Internal()->Name(), name)){
					return pVar;
				}
				pVar = pVar->Next();
			}
			return NULL;
		}
		static gameVarsList* Singleton();
};
HRESULT (WINAPI* origReset)(LPDIRECT3DDEVICE9, D3DPRESENT_PARAMETERS*); //16
HRESULT (WINAPI* origEndScene)(LPDIRECT3DDEVICE9); //42
HRESULT (WINAPI* origDIP)(LPDIRECT3DDEVICE9, D3DPRIMITIVETYPE, INT, UINT, UINT, UINT, UINT); //82
//HRESULT (WINAPI* origCreateTex)(LPDIRECT3DDEVICE9, UINT, UINT, UINT, DWORD, D3DFORMAT, D3DPOOL, IDirect3DTexture9**, HANDLE*); //23
HRESULT (WINAPI* origGetRendTD)(LPDIRECT3DDEVICE9, IDirect3DSurface9*, IDirect3DSurface9*); //32

// Define our global variable pointers here

gameVarsClass* pGrassVar;
gameVarsClass* pTreesVar;
gameVarsClass* pCullingVar;

void DrawTxt(int x, int y, DWORD color, const char *fmt, ...){
	RECT FontPos = { x, y, x + 120, y + 16 };
	char buf[1024] = {'\0'};
	va_list va_alist;

	va_start(va_alist, fmt);
	vsprintf(buf, fmt, va_alist);
	va_end(va_alist);
	
	pFont->DrawText(NULL, buf, -1, &FontPos, DT_NOCLIP, color); //DT_NOCLIP
}
void SetColor(LPDIRECT3DDEVICE9 pDevice, float r, float g, float b, float a, float glowr, float glowg, float glowb, float glowa){
	float lightValues[4] = {r, g, b, a};
	float glowValues[4] = {glowr, glowg, glowb, glowa};

	pDevice->SetPixelShaderConstantF(1, lightValues, 1);
	pDevice->SetPixelShaderConstantF(3, glowValues, 1);
}
//Cham It
void CH_IT(LPDIRECT3DDEVICE9 pDev, D3DPRIMITIVETYPE Type, INT BaseIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PCount, float r, float g, float b, float r2, float g2, float b2){
	pDev->SetRenderState(D3DRS_ZENABLE, FALSE);
	pDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	SetColor(pDev, r2, g2, b2, 1.0f, r2, g2, b2, 1.0f);	//Not visible color
	origDIP( pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount );
	pDev->SetRenderState(D3DRS_ZENABLE, TRUE);
	pDev->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	SetColor(pDev, r, g, b, 1.0f, r, g, b, 1.0f);			//Visible color
}
MODULEENTRY32 getModule(int pid){
	MODULEENTRY32 module;
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE,pid);
	module.dwSize = sizeof(MODULEENTRY32);//MSDN says we have to set this to sizeof(MODULEENTRY32)
	if( snapshot == INVALID_HANDLE_VALUE ){
		CloseHandle(snapshot);
		_cprintf("CreateToolhelp32Snapshot failed!\n");
	}
	if( Module32First(snapshot,&module) == FALSE ){
		CloseHandle(snapshot);
		_cprintf("Module32First failed!\n");
	}
	//_cprintf("Module exe is: %s\n", module.szModule);
	CloseHandle(snapshot);
	return module;
}

DWORD findAddr(HANDLE proc, MODULEENTRY32 module, BYTE* asmBytes, char* byteMask){
	BYTE* procMemory = new BYTE[module.modBaseSize];
	DWORD bytesRead, maskLen, sOffset;
	int found = 0;
	maskLen = strlen(byteMask);
	ReadProcessMemory(proc, (void*)module.modBaseAddr, (void*)procMemory, module.modBaseSize, &bytesRead);
	//_cprintf("%i\n", GetLastError());
	//_cprintf("Entering findAddr loop\t%c\n\n", byteMask[0]);
	for (DWORD i=0; i<bytesRead; ++i){// For loop searching all process memory
		if (procMemory[i] == asmBytes[0]){
			found = 1; sOffset = 1; // We have the initial match	
			for (DWORD a=1; a<maskLen; ++a){// For loop searching after initial match
				if ( byteMask[a] == 'x' && procMemory[i+a] != asmBytes[sOffset++] ){
					found = 0; break;
				}
			}
			if (found){
				//_cprintf("Found it!\t0x%x\n",module.modBaseAddr+i);
				delete(procMemory); return (DWORD)module.modBaseAddr + i;
			}
		}
	}
	_cprintf("Failed to find address.\t%i\n", bytesRead);
	delete(procMemory);
}
gameVarsList* gameVarsList::Singleton(){
	static DWORD varList = 0;
	if(varList == 0){
		MODULEENTRY32 module = getModule(GetCurrentProcessId());
		HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, GetCurrentProcessId());
		//_cprintf("%i\t0x%x\t%i\n", GetCurrentProcessId(), module.modBaseAddr, module.modBaseSize);
		varList = findAddr(proc, module, (BYTE*)"\x56\x57\x8B\xF0\x68\x80\x00\x00\x00", (char*)"xxxxxxxxx");
		_cprintf("gameVarSingleton: 0x%x\n", varList);
		CloseHandle(proc);
		if(varList == 0)
			MessageBox(0, "findAddr failed!", "dafuk?", MB_OK);
		varList = varList - 4;
		varList = *(DWORD*)varList;
	}
	return (gameVarsList*) *(DWORD*) varList;
}

HRESULT WINAPI DIP( LPDIRECT3DDEVICE9 pDev, D3DPRIMITIVETYPE Type, INT BaseIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PCount ){
	/*if(GetAsyncKeyState(VK_HOME)&1){				//HOME to toggle trees & grass
		_cprintf("VK_HOME\n");
		varsTrue = !varsTrue;
		if(pTreesVar && pGrassVar){
			int val = varsTrue ? 1 : 0;
			if(pTreesVar->Get<int>() != val)
				pTreesVar->Set<int>(val);
			if(pGrassVar->Get<int>() != val)
				pGrassVar->Set<int>(val);
		}
	}*/
	if(somethingIsOn){
	LPDIRECT3DVERTEXBUFFER9 Stream_Data;
	UINT Offset = 0;
	UINT Stride = 0;
	
	BYTE* pRetAddrBytes = (BYTE*) __builtin_return_address(0);
	pRetAddrBytes -= 0x2F; //Point us to the asm("shr eax, 1F"); (C1 E8 1F)
	
	if( pDev->GetStreamSource(0, &Stream_Data, &Offset, &Stride) == D3D_OK )
		Stream_Data->Release();
		
	if(pRetAddrBytes[0] == 0xC1 && pRetAddrBytes[1] == 0xE8 && pRetAddrBytes[2] == 0x1F){ //Making sure we're in the right function
		asm("movl %%ebx, %0":"=d"(dwEBX));//%0 is output, %1 is input, = is write only, [a-d] is e*x register to use, r is any register?, 
		DWORD tmp = *dwEBX;
		char* modelName = (char*)(tmp);
		if(GetAsyncKeyState(VK_F12) != 0){
			_cprintf("Model: %s\n", modelName);
		}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		if(GetAsyncKeyState(VK_DELETE)!=0){				//DELETE to toggle Player & Zombie Chams
			if(strncasecmp(modelName, "Hero_", 5) == 0)	//Players
				CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 0.5, 255.0, 0.5, 100.0, 0.5, 100.0); //Green visible / Purple behind
			if(strncasecmp(modelName, "Character", 9) == 0)
				CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 0.5, 255.0, 0.5, 100.0, 0.5, 100.0);
			if(strncasecmp(modelName, "Gear_", 5) == 0)		//Backpack
				CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 255.0, 0.5, 0.5, 255.0, 0.5, 0.5);	 //Red
			if(strncasecmp(modelName, "Zombie_", 7) == 0)	//Zombies
				CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 0.1, 0.1, 100.0, 0.1, 0.1, 100.0);   //Blue
		}
		if(GetAsyncKeyState(VK_INSERT) != 0){				//INSERT to toggle Item & Weapon Chams
			if( strncasecmp(modelName, "SHG_", 4) == 0 ||			// Shotguns
				strncasecmp(modelName, "SMG_", 4) == 0 ||			// Submachine Guns
				strncasecmp(modelName, "MG_", 3) == 0 ||			// Machine Guns
				strncasecmp(modelName, "ASR_", 4) == 0 ||			// Assault Rifles
				strncasecmp(modelName, "SNP_", 4) == 0 ||			// Sniper Rifles
				strncasecmp(modelName, "MEL_", 4) == 0 ||			// Melee
				strncasecmp(modelName, "HG_", 3) == 0 ||			// Hand Guns
				strncasecmp(modelName, "EXP_", 4) == 0 ||			// Explosives
				strncasecmp(modelName, "ATTM_", 5) == 0 ||			// Attachments
				strncasecmp(modelName, "SUP_", 4) == 0 ){			// Suppressors
					CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 100.0, 50.0, 0.1, 100.0, 50.0, 0.1);//Yellow
			}else if(strncasecmp(modelName, "Consumables_", 12)==0||// Consumables
				strncasecmp(modelName, "Consumable_", 11) == 0 ||	// Consumables
				strncasecmp(modelName, "Item_", 5) == 0 ||			// Items (Item_ and item_)
				strncasecmp(modelName, "Loot_", 5) == 0 ){			// Loot
					CH_IT(pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount, 0.5, 100.0, 200.0, 0.5, 100.0, 200.0);//Blueish White
			} else {
				//_cprintf("Unknown Item: %s\n", (char*)modelName);
			}
		}
		if(GetAsyncKeyState(VK_END) != 0){			//End to toggle buildings
			if(Stride==20){
				//_cprintf("Unknown Item: %s\n", (char*)modelName);
				return D3D_OK;
			}
		}
	}
	if(GetAsyncKeyState(VK_HOME) != 0){				//HOME to toggle grass
		if( (NumVertices == 2040 && PCount == 1530) ||
		(NumVertices == 352 && PCount == 176) ||
		(NumVertices == 2360 && PCount == 1180) ||
		(NumVertices == 28800 && PCount == 14400) ||
		(NumVertices == 4284 && PCount == 2142) ||
		(NumVertices == 1888 && PCount == 944) ||
		(NumVertices == 5600 && PCount == 2800) ||
		(NumVertices == 10000 && PCount == 5000) ||
		(NumVertices == 1800 && PCount == 900) ||
		(NumVertices == 1175 && PCount == 611) ||
		(NumVertices == 2000 && PCount == 1000) ||
		(NumVertices == 3264 && PCount == 1632) ||
		(NumVertices == 1440 && PCount == 720) ||
		(NumVertices == 320 && PCount == 160) ||
		(NumVertices == 1680 && PCount == 1260) ||
		(NumVertices == 480 && PCount == 240) ||
		(NumVertices == 360 && PCount == 180) ||
		(NumVertices == 1160 && PCount == 580) ||
		(NumVertices == 928 && PCount == 464) ||
		(NumVertices == 160 && PCount == 80) ||
		(NumVertices == 2124 && PCount == 1062) ||
		(NumVertices == 14400 && PCount == 7200) ||
		(NumVertices == 5000 && PCount == 2500) ||
		(NumVertices == 960 && PCount == 720) ||
		(NumVertices == 575 && PCount == 299) ||
		(NumVertices == 1632 && PCount == 816) ||
		(NumVertices == 1000 && PCount == 500) ||
		(NumVertices == 2800 && PCount == 1400) ||
		(NumVertices == 720 && PCount == 360) ||
		(NumVertices == 900 && PCount == 450) ||
		(NumVertices == 784 && PCount == 588) ||
		(NumVertices == 240 && PCount == 120) ||
		(NumVertices == 180 && PCount == 90) ){ return D3D_OK; }
	}
	if(GetAsyncKeyState(VK_PRIOR) != 0){				//PG UP to toggle world
		if(Stride==4){
			pDev->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
		}
	}
	return origDIP( pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount );
	}
	return origDIP( pDev, Type, BaseIndex, MinIndex, NumVertices, StartIndex, PCount );
}

HRESULT WINAPI RESET( LPDIRECT3DDEVICE9 pDev, D3DPRESENT_PARAMETERS* PParameters ){if(somethingIsOn){
	MessageBox(NULL, "RESET CALLED!", "Shits goin' crash son.", MB_OK);
	return D3DERR_DEVICENOTRESET;
	}
	return origReset( pDev, PParameters );
}

HRESULT WINAPI ENDSCENE( LPDIRECT3DDEVICE9 pDev ){if(somethingIsOn){
	pDev->GetViewport( &Viewport );
	if(pFont == NULL) D3DXCreateFont(pDev, 15, 0, FW_BOLD, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, ANTIALIASED_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Verdana", &pFont);

	if(GetKeyState(VK_NEXT)){ 				// PG DOWN Toggle Crosshairs
		DWORD ScrX = Viewport.Width / 2; 
		DWORD ScrY = (Viewport.Height / 2)+150; 
		{RectA.x1 = ScrX - 4;}	{RectA.y1 = ScrY - 1;}
		{RectA.x2 = ScrX + 3;}	{RectA.y2 = ScrY;}
		{RectB.x1 = ScrX - 1;}	{RectB.y1 = ScrY - 4;}
		{RectB.x2 = ScrX;}		{RectB.y2 = ScrY + 3;}
		pDev->Clear( 1, &RectA, D3DCLEAR_TARGET, D3DCOLOR_ARGB( 255, 40, 255, 40 ), 0,  0 );
		pDev->Clear( 1, &RectB, D3DCLEAR_TARGET, D3DCOLOR_ARGB( 255, 40, 255, 40 ), 0,  0 ); 
	}

	/*if(pFont != NULL){
		int y = 0;
		if(GetAsyncKeyState(VK_DELETE) != 0){
			DrawTxt(0,y, 0xFF00FF00, "DEL");
			y+=15;
		}if(GetAsyncKeyState(VK_INSERT) != 0){
			DrawTxt(0,y, 0xFF00FF00, "INS");
			y+=15;
		}if(GetAsyncKeyState(VK_END) != 0){
			DrawTxt(0,y, 0xFF00FF00, "END");
			y+=15;
		}if(pGrassVar->Get<int>()==0||pTreesVar->Get<int>()==0){
			DrawTxt(0,y, 0xFF00FF00, "HOME");
			y+=15;
		}
		if(GetKeyState(VK_NEXT)){		// Xhairs
			DrawTxt(0,y, 0xFF00FF00, "PGD");
			y+=15;
		}if(GetAsyncKeyState(VK_PRIOR) != 0){
			DrawTxt(0,y, 0xFF00FF00, "PGU");
			y+=15;
		}
	}*/

	return origEndScene( pDev );
	}
	return origEndScene( pDev );
}

HRESULT WINAPI GETRENDERTARGETDATA( LPDIRECT3DDEVICE9 pDev, IDirect3DSurface9 *pRenderTarget, IDirect3DSurface9 *pDestSurface ){
	_cprintf("GetRendTD: 0x%x 0x%x\n", pRenderTarget, pDestSurface);
	if(somethingIsOn){ return D3D_OK; }
	return origGetRendTD( pDev, pRenderTarget, pDestSurface );
}

VOID WINAPI THREAD( VOID ){
	*(PDWORD)&origEndScene = pVTable[42];
	*(PDWORD)&origReset	= pVTable[16];
	*(PDWORD)&origDIP = pVTable[82];
	*(PDWORD)&origGetRendTD = pVTable[32];
	//_cprintf("oEndScene:0x%x\n oReset:0x%x\n oDIP:0x%x\n",pVTable[42],origReset,origDIP);
	
	gameVarsList* pVarsList = gameVarsList::Singleton();
	if(pVarsList){ // Set our global variable pointers here
		_cprintf("pVarList: 0x%x\n", pVarsList);
		pGrassVar = (gameVarsClass*) pVarsList->Get("r_grass_draw");
		_cprintf("GrassVar: 0x%x\n", pGrassVar);
		//pTreesVar = (gameVarsClass*) pVarsList->Get("g_trees");
		//pCullingVar = (gameVarsClass*) pVarsList->Get("r_use_oq");
	}if(pGrassVar){// && pTreesVar && pCullingVar){
		varStatus.grassVar = pGrassVar->Get<int>() ? true : false;
		//varStatus.treesVar = pTreesVar->Get<int>() ? true : false;
		//varStatus.ocullVar = pCullingVar->Get<int>() ? true : false;
	}else{MessageBox(0, "pVarsList is failing!!!\n", "...", MB_OK);}
	if(varStatus.grassVar||varStatus.treesVar){varsTrue=true;}
	_cprintf("varStatus: Grass:%i Trees:%i Culling:%i\n", varStatus.grassVar, varStatus.treesVar, varStatus.ocullVar);
	
	while( 1 ){
		pVTable[42] = (DWORD)ENDSCENE;
		pVTable[16] = (DWORD)RESET;
		pVTable[82] = (DWORD)DIP;
		pVTable[32] = (DWORD)GETRENDERTARGETDATA;
		if( GetAsyncKeyState(VK_HOME)!=0 || GetAsyncKeyState(VK_DELETE)!=0 || GetAsyncKeyState(VK_INSERT)!=0 || GetAsyncKeyState(VK_END)!=0 || GetKeyState(VK_NEXT) || GetAsyncKeyState(VK_PRIOR)!=0 ){
			somethingIsOn = true; } else { somethingIsOn = false; }
		Sleep( 100 );
	}
}

extern "C" BOOL WINAPI DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpvReserved ){
	if( dwReason == DLL_PROCESS_ATTACH ){
		DisableThreadLibraryCalls( hModule );
		AllocConsole();
		_cprintf("\n~~~~~~~~~~~~~~~~~~\n");
		while(!GetModuleHandle("d3d9")){
			//_cprintf("Waiting on D3D9.dll...\n");
			Sleep(250);
		}
		
		DWORD exeBaseAddr = (DWORD)GetModuleHandle(NULL);
		if(!exeBaseAddr){
			MessageBox(NULL,"Can't get BaseAddr", "...", MB_OK);
			exit(0);
		}
		_cprintf("Base Addr:0x%x\n",(DWORD)exeBaseAddr);
		_cprintf("Render Addr:0x%x\n", (DWORD)(exeBaseAddr + RENDERCLASS_OFFSET ));
		d3renderClass *renderClass = (d3renderClass*)(*(PDWORD)( exeBaseAddr + RENDERCLASS_OFFSET ));
		_cprintf("renderClass:0x%x\n",renderClass);
		pDevice = (LPDIRECT3DDEVICE9)(DWORD*)*(DWORD*)renderClass->pD3dev;
		_cprintf("pDev:0x%x\n",(DWORD)pDevice);
		pVTable = (DWORD*)pDevice;
		patchThread = CreateThread( NULL ,0, (LPTHREAD_START_ROUTINE)THREAD, NULL, 0,NULL );
		return TRUE;
	}
	
	if( dwReason == DLL_PROCESS_DETACH ){
		//FreeConsole();
		TerminateThread(patchThread, 0);
		pVTable[42] = (DWORD)origEndScene;
		pVTable[16] = (DWORD)origReset;
		pVTable[82] = (DWORD)origDIP;
		pVTable[32] = (DWORD)origGetRendTD;
		CloseHandle(patchThread);
		return TRUE;
	}
	return TRUE;
}